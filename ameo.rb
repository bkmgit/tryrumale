require 'csv'
require 'numo/narray'
require 'numo/gnuplot'
require 'numo/gsl'
require 'rumale'

train_data = CSV.read("datachallenge_cods2016/datachallenge_cods2016/train.csv",
                 headers: true)
train_data = train_data.by_col!
keep = ["Salary","JobCity","Gender","12graduation",
        "CollegeTier","GraduationYear","collegeGPA", 
        "English", "Logical", "Quant", "conscientiousness", 
        "agreeableness", "extraversion", "nueroticism", 
        "openess_to_experience"]

train_data = train_data.delete_if do |column_name,column_values|
  !keep.include? column_name
end


train_data = train_data.by_row!
# Remove rows where JobCity or State GDP per Capita is unknown
train_data.delete_if {|row| row['JobCity'] == '-1'}
train_data.delete_if {|row| row['JobCity'] == 'AM'}
train_data.delete_if {|row| row['JobCity'] == 'india'}
train_data.delete_if {|row| !!(row['JobCity'] =~ /daman/i) }

train_data = train_data.by_col!

job_city = train_data['JobCity']
gender = train_data['Gender']
graduation_year = train_data['GraduationYear']

def tolower array
  array.each_with_index do |n,i|
    array[i] = n.downcase
  end
  return array
end

job_city = tolower job_city
job_city = job_city.map { |entry| entry.strip }
train_data['JobCity'] = job_city

gender = tolower gender
gender = gender.map { |entry| entry.gsub("m","1") }
gender = gender.map { |entry| entry.gsub("f","0") }
train_data['Gender'] = gender
# Print headers
headers = train_data.headers 
p headers

# Remove data from outside India
train_data = train_data.by_row!
train_data.delete_if {|row| row['JobCity'] == 'dammam'}
train_data.delete_if {|row| row['JobCity'] == 'dubai'}
train_data.delete_if {|row| row['JobCity'] == 'jeddah saudi arabia'}
train_data.delete_if {|row| row['JobCity'] == 'johannesburg'}
train_data.delete_if {|row| row['JobCity'] == 'kalmar, sweden'}
train_data.delete_if {|row| row['JobCity'] == 'london'}
train_data.delete_if {|row| row['JobCity'] == 'ras al khaimah'}
train_data.delete_if {|row| row['JobCity'] == 'al jubail,saudi arabia'}
train_data.delete_if {|row| row['JobCity'] == 'australia'}
train_data.delete_if {|row| row['12graduation'] == '0'}
train_data.delete_if {|row| row['GraduationYear'] == '0'}

train_data.by_col!

# Create Hash of city and state
city_state_hash = {
  "a-64,sec-64,noida" => "Uttar Pradesh",
  "agra" => "Uttar Pradesh",
  "ahmedabad" => "Gujarat",
  "ahmednagar" => "Maharashtra",
  "allahabad" => "Uttar Pradesh",
  "alwar" =>  "Rajasthan",
  "ambala" => "Haryana",
  "ambala city" => "Haryana",
  "angul" => "Haryana",
  "ariyalur" => "Tamil Nadu",
  "asansol" => "West Bengal",
  "asifabadbanglore" => "Karnataka",
  "aurangabad" => "Maharashtra",
  "baddi hp" => "Himachal Pradesh",
  "bahadurgarh" => "Haryana",
  "banagalore" => "Karnataka",
  "banaglore" => "Karnataka",
  "bangalore" => "Karnataka",
  "banglore" => "Karnataka",
  "bankura" => "West Bengal",
  "bareli" => "Madhya Pradesh",
  "baripada" => "Odisha",
  "baroda" => "Gujarat",
  "bathinda" => "Punjab",
  "beawar" => "Rajasthan",
  "belgaum" => "Karnataka",
  "bellary" => "Karnataka",
  "bengaluru" => "Karnataka",
  "bhagalpur" => "Bihar",
  "bharuch" => "Gujarat",
  "bhilai" => "Chhattisgarh",
  "bhiwadi" => "Rajasthan",
  "bhopal" => "Madhya Pradesh",
  "bhubaneshwar" => "Odisha",
  "bhubaneswar" => "Odisha",
  "bhubneshwar" => "Odisha",
  "bihar" => "Bihar",
  "bikaner" => "Rajasthan",
  "bilaspur" => "Himachal Pradesh",
  "bulandshahar" => "Uttar Pradesh",
  "bundi" => "Rajasthan",
  "burdwan" => "West Bengal",
  "calicut" => "Kerala",
  "chandigarh" => "Punjab",
  "chandrapur" => "Maharashtra",
  "chennai & mumbai" => "Tamil Nadu",
  "chennai, bangalore" => "Tamil Nadu",
  "chennai" => "Tamil Nadu",
  "cheyyar" => "Tamil Nadu",
  "coimbatore" => "Tamil Nadu",
  "dausa" => "Rajasthan",
  "dehradun" => "Uttarakhand",
  "delhi/ncr" => "Delhi",
  "delhi" => "Delhi",
  "dhanbad" => "Jharkhand",
  "dharamshala" => "Himachal Pradesh",
  "dharmapuri" => "Madhya Pradesh",
  "dharuhera" => "Haryana",
  "durgapur" => "West Bengal",
  "ernakulam" => "Kerala",
  "faridabad" => "Haryana",
  "gagret" => "Himachal Pradesh",
  "gajiabaad" => "Uttar Pradesh",
  "gandhi nagar" => "Gujarat",
  "gandhinagar" => "Gujarat",
  "ganjam" => "Odisha",
  "gaziabaad" => "Uttar Pradesh",
  "ghaziabad" => "Uttar Pradesh",
  "gonda" => "Uttar Pradesh",
  "gorakhpur" => "Uttar Pradesh",
  "greater noida" => "Uttar Pradesh",
  "gulbarga" => "Karnataka",
  "guragaon" => "Haryana",
  "gurgaon" => "Haryana",
  "gurgoan" => "Haryana",
  "gurga" => "Haryana",
  "guwahati" => "Assam",
  "gwalior" => "Madhya Pradesh",
  "haldia" => "West Bengal",
  "haridwar" => "Uttarakhand",
  "haryana" => "Haryana",
  "hderabad" => "Telangana",
  "hissar" => "Haryana",
  "hospete" => "Karnataka",
  "howrah" => "West Bengal",
  "hubli" => "Karnataka",
  "hyderabad(bhadurpally)" => "Telangana",
  "hyderabad" => "Telangana",
  "indirapuram, ghaziabad" => "Uttar Pradesh",
  "indore" => "Madhya Pradesh",
  "jabalpur" => "Madhya Pradesh",
  "jagdalpur" => "Chhattisgarh",
  "jaipur" => "Rajasthan",
  "jalandhar" => "Punjab",
  "jammu" => "Jammu and Kashmir",
  "jamnagar" => "Gujarat",
  "jamshedpur" => "Jharkhand",
  "jaspur" => "Uttarakhand",
  "jhajjar" => "Haryana",
  "jhansi" => "Uttar Pradesh",
  "jodhpur" => "Rajasthan",
  "joshimath" => "Uttarakhand",
  "jowai" => "Meghalaya",
  "kakinada" => "Andhra Pradesh",
  "kala amb" => "Himachal Pradesh",
  "kanpur" => "Uttar Pradesh",
  "karad" => "Maharashtra",
  "karnal" => "Haryana",
  "keral" => "Kerala",
  "kharagpur" => "West Bengal",
  "khopoli" => "Maharashtra",
  "kochi/cochin, chennai and coimbatore" => "Kerala",
  "kochi/cochin" => "Kerala",
  "kochi" => "Kerala",
  "kolhapur" => "Maharashtra",
  "kolkata`" => "West Bengal",
  "kolkata" => "West Bengal",
  "kota" => "Rajasthan",
  "kudankulam ,tarapur" => "Tamil Nadu",
  "kurnool" => "Andhra Pradesh",
  "latur (maharashtra )" => "Maharashtra",
  "lucknow" => "Uttar Pradesh",
  "ludhiana" => "Punjab",
  "madurai" => "Tamil Nadu",
  "maharajganj" => "Uttar Pradesh",
  "mainpuri" => "Uttar Pradesh",
  "manesar" => "Haryana",
  "mangalore" => "Karnataka",
  "meerut" => "Uttar Pradesh",
  "mettur, tamil nadu" => "Tamil Nadu",
  "miryalaguda" => "Telangana",
  "mohali" => "Punjab",
  "mumbai" => "Maharashtra",
  "muvattupuzha" => "Kerala",
  "muzaffarnagar" => "Uttar Pradesh",
  "muzaffarpur" => "Bihar",
  "muzzafarpur" => "Bihar",
  "mysore" => "Karnataka",
  "nagari" => "Andhra Pradesh",
  "nagpur" => "Maharashtra",
  "nalagarh" => "Himachal Pradesh",
  "nanded" => "Maharashtra",
  "nashik" => "Maharashtra",
  "nasikcity" => "Maharashtra",
  "navi mumbai , hyderabad" => "Maharashtra",
  "navi mumbai" => "Maharashtra",
  "ncr" => "Delhi",
  "neemrana" => "Rajasthan",
  "nellore" => "Andhra Pradesh",
  "new delhi - jaisalmer" => "Delhi",
  "new dehli" => "Delhi",
  "new delhi" => "Delhi",
  "noida" => "Uttar Pradesh",
  "nouda" => "Uttar Pradesh",
  "ongole" => "Andhra Pradesh",
  "orissa" => "Odisha",
  "panchkula" => "Haryana",
  "pantnagar" => "Uttarakhand",
  "patiala" => "Punjab",
  "patna" => "Bihar",
  "phagwara" => "Punjab",
  "pilani" => "Rajasthan",
  "pondicherry" => "Puducherry",
  "pondi" => "Puducherry",
  "pondy" => "Puducherry",
  "punchkula" => "Haryana",
  "pune" => "Maharashtra",
  "punr" => "Maharashtra",
  "rae bareli" => "Uttar Pradesh",
  "raigarh" => "Chhattisgarh",
  "raipur" => "Chhattisgarh",
  "rajasthan" => "Rajasthan",
  "rajkot" => "Gujarat",
  "rajpura" => "Punjab",
  "ranchi" => "Jharkhand",
  "ratnagiri" => "Maharashtra",
  "rayagada, odisha" => "Odisha",
  "rewari" => "Haryana",
  "rohtak" => "Haryana",
  "roorkee" => "Uttarakhand",
  "rourkela" => "Odisha",
  "rudrapur" => "Uttarakhand",
  "sadulpur,rajgarh,distt-churu,rajasthan" => "Rajasthan",
  "sahibabad" => "Uttar Pradesh",
  "salem" => "Uttar Pradesh",
  "sambalpur" => "Odisha",
  "sampla" => "Haryana",
  "secunderabad" => "Telangana",
  "shahdol" => "Madhya Pradesh",
  "shahibabad" => "Uttar Pradesh",
  "shimla" => "Himachal Pradesh",
  "siliguri" => "West Bengal",
  "singaruli" => "Madhya Pradesh",
  "sonepat" => "Haryana",
  "sonipat" => "Haryana",
  "surat" => "Gujarat",
  "technopark, trivandrum" => "Kerala",
  "thane" => "Maharashtra",
  "thiruvananthapuram" => "Kerala",
  "tirunelvelli" => "Tamil Nadu",
  "tirupathi" => "Andhra Pradesh",
  "tirupati" => "Andhra Pradesh",
  "tornagallu" => "Karnataka",
  "trichur" => "Kerala",
  "trichy" => "Tamil Nadu",
  "trivandrum" => "Kerala",
  "udaipur" => "Rajasthan",
  "una" => "Himachal Pradesh",
  "unnao" => "Uttar Pradesh",
  "vadodara" => "Gujarat",
  "vandavasi" => "Tamil Nadu",
  "vapi" => "Gujarat",
  "varanasi" => "Uttar Pradesh",
  "vellore" => "Tamil Nadu",
  "vijayawada" => "Andhra Pradesh",
  "visakhapatnam" => "Andhra Pradesh",
  "vizag" => "Andhra Pradesh",
  "vsakhapttnam" => "Andhra Pradesh",
  "yamuna nagar" => "Haryana"
}

# change city to state
job_city = train_data['JobCity']

# ensure regex works on full entry to avoid
# changes like "new dehli" becoming "new New Delhi"
city_state_hash.each do |key, value|
  job_city.each do |entry|
    entry.gsub!(/^#{Regexp.escape(key)}$/,value)
  end
end

# create hash of state and GDP per capita in 2015/2016
state_income_hash = {
  "Goa"                         => "334575",
  "Delhi"                       => "328985",
  "Sikkim"                      => "245987",
  "Chandigarh"                  => "230417",
  "Haryana"                     => "164963",
  "Puducherry"                  => "172727",
  "Karnataka"                   => "148108",
  "Tamil Nadu"                  => "140840",
  "Kerala"                      => "148133",
  "Uttarakhand"                 => "147592",
  "Gujarat"                     => "139354",
  "Telangana"                   => "140441",
  "Maharashtra"                 => "146258",
  "Himachal Pradesh"            => "135512",
  "Andaman and Nicobar Islands" => "126995",
  "Mizoram"                     => "114055",
  "Punjab"                      => "118558",
  "Andhra Pradesh"              => "108002",
  "Arunachal Pradesh"           => "112046",
  "Nagaland"                    => "82466",
  "Tripura"                     => "83680",
  "Rajasthan"                   => "83426",
  "West Bengal"                 => "75592",
  "Chhattisgarh"                => "73590",
  "Odisha"                      => "64595",
  "Jammu and Kashmir"           => "73215",
  "Madhya Pradesh"              => "62626",
  "Meghalaya"                   => "68836",
  "Assam"                       => "60817",
  "Jharkhand"                   => "52754",
  "Manipur"                     => "55447",
  "Uttar Pradesh"               => "47118",
  "Bihar"                       => "30404"
}
state_income_hash.each do |key, value|
  job_city = job_city.map { |entry| entry.gsub(/^#{Regexp.escape(key)}$/,value) }
end

train_data['JobCity'] = job_city
train_data.by_row!
train_data_n = Numo::DFloat.zeros(3522,15)
# Convert to array

i = 0
for row in train_data
  j = 0
  for cell in row
    train_data_n[i,j] = cell[1].to_f()
    j +=1
  end
  i += 1
end

# Make plots
def makeplot(fname,square,xlab,ylab,xdata,ydata)
  if square == 1
    sst = "set 'size square'"
  else
    sst = ""
  end
  Numo.gnuplot.reset
  Numo.gnuplot.set term:"png"
  fimg = fname+'.png'
  Numo.gnuplot.set output:fimg
  Numo.gnuplot do
    set :nokey
    set :xtics
    set xlabel:xlab
    set ylabel:ylab
    set 'xtics rotate'
    eval(sst)
    unset :colorbox
    plot xdata,ydata,with:"points", pt:7, ps:1, lc_rgb:"blue"
  end
end

def get_quartiles(data)
  data_sort = data.sort
  q1 = Numo::GSL::Stats.quantile_from_sorted_data(data_sort,0.25)
  q2 = Numo::GSL::Stats.quantile_from_sorted_data(data_sort,0.5)
  q3 = Numo::GSL::Stats.quantile_from_sorted_data(data_sort,0.75)
  return q1, q2, q3
end

def makebiboxplot(fname,xlab,ylab,label1,label2,data1,data2)
  data1_q1, data1_q2, data1_q3 = get_quartiles(data1)
  fn = "tmp1.dat"
  f = open(fn,"w")
  f.printf("0 %f %f %f %f %f\n",
           data1.min,
           data1_q1,
           data1_q2,
           data1_q3,
           data1.max)
  f.close
  data2_q1, data2_q2, data2_q3 = get_quartiles(data2)
  fn = "tmp2.dat"
  f = open(fn,"w")
  f.printf("1 %f %f %f %f %f\n",
          data2.min,
          data2_q1,
          data2_q2,
          data2_q3,
          data2.max)
  f.close
  Numo.gnuplot.reset
  Numo.gnuplot.set term:"png"
  fimg = fname+'_boxplot.png'
  Numo.gnuplot.set output:fimg
  Numo.gnuplot do
    set :nokey
    set boxwidth:0.2
    set 'xtics ("' + label1 + '" 0, "' + label2 + '" 1)'
    set ylabel:ylab
    set xlabel:xlab
    set xrange:[-1..2]
    unset :colorbox
    plot ["'tmp1.dat'", using:[1,3,2,6,5],
         with:"candlesticks", lt:3, lw:2, whiskerbars:true  ],
         ["''", using:[1,4,4,4,4], with:"candlesticks",
         lt:-1, lw:2, notitle:true],
         ["'tmp2.dat'", using:[1,3,2,6,5],
         with:"candlesticks", lt:3, lw:2, whiskerbars:true  ],
         ["''", using:[1,4,4,4,4], with:"candlesticks",
         lt:-1, lw:2, notitle:true]
  end
  File.delete("tmp1.dat")
  File.delete("tmp2.dat")
end

def makemultiboxplot(fname,xlab,ylab,xdata,ydata)
  labels = ((xdata.to_a).uniq).sort
  maxindex = labels.size - 1
  for x in 0..maxindex do
    values = Numo::DFloat.zeros((xdata.eq((labels[x]).to_f)).count)
    count = 0
    (xdata.size).times do |i|
      if (xdata[i]).to_i == (labels[x]).to_i 
        values[count]=ydata[i]
        count += 1
      end
    end
    if count > 0
      values_q1, values_q2, values_q3 = get_quartiles(values)
      values_min = values.min
      values_max = values.max
    else
      values_min = 0
      values_q1 = 0
      values_q2 = 0
      values_q3 = 0
      values_max = 0
    end
    # create temporary file with data for each section
    fn = "tmp" + ((labels[x]).to_i).to_s + ".dat"
    f = open(fn,"w")
    f.printf("%d %f %f %f %f %f\n",
          x,
          values_min,
          values_q1,
          values_q2,
          values_q3,
          values_max)
    f.close
  end
  # Make plots
  ## Assemble main plot command
  plotst = "plot "
  for x in 0..maxindex do
    fn = "tmp" + ((labels[x]).to_i).to_s + ".dat"
    if x == maxindex 
      plotst = plotst + "['" +fn+"', using:[1,3,2,6,5],with:'candlesticks', 
                               lt:3, lw:2, whiskerbars:true  ],
            ['', using:[1,4,4,4,4], with:'candlesticks',lt:-1, lw:2, notitle:true]"
    else
      plotst = plotst + "['" +fn+"', using:[1,3,2,6,5],with:'candlesticks', 
                             lt:3, lw:2, whiskerbars:true  ],
            ['', using:[1,4,4,4,4], with:'candlesticks',lt:-1, lw:2, notitle:true],"
    end
  end
  ## x axis labels
  xticst = 'set \'xtics("  " -1, '
  for x in 0..maxindex do
    xticst = xticst + '"' + ((labels[x]).to_i).to_s + '" ' + x.to_s + ' , '
  end
  xticst = xticst + '"  " ' + (maxindex+1).to_s + ' )\''
  Numo.gnuplot.reset
  Numo.gnuplot.set term:"png"
  fimg = fname+'_boxplot.png'
  Numo.gnuplot.set output:fimg
  Numo.gnuplot do
    set :nokey
    set boxwidth:0.2
    set ylabel:ylab
    set xlabel:xlab
    set xrange:[-1..maxindex+1]
    set 'xtics rotate'
    eval(xticst)
    unset :colorbox
    eval(plotst)
  end
  # Clean up temporary files
  for x in 0..maxindex do
    fn = "tmp" + ((labels[x]).to_i).to_s + ".dat"
    File.delete(fn)
  end
end

fname='OpennessToExperience_Salary'
xlab='Openness to Experience'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,14]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='Neuroticism_Salary'
xlab='Neuroticism'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,13]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='Extraversion_Salary'
xlab='Extraversion'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,12]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='Agreeableness_Salary'
xlab='Agreeableness'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,11]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='Conscientiousness_Salary'
xlab='Conscientiousness'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,10]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='QuantitativeAptitude_Salary'
xlab='Quantitative Aptitude'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,9]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='LogicalAptitude_Salary'
xlab='Logical Aptitude'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,8]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='EnglishAptitude_Salary'
xlab='English Aptitude'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,7]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='GraduationYear_Salary'
xlab='College Graduation Year'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,6]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)
makemultiboxplot(fname,xlab,ylab,xdata,ydata)

fname='CollegeGPA_Salary'
xlab='College GPA'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,5]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)

fname='CollegeTier_Salary'
xlab='College Tier'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,4]
ydata=train_data_n[0..-1,0]
makeplot(fname,square,xlab,ylab,xdata,ydata)
# Separate data by tier to create boxplot
tier1 = Numo::DFloat.zeros(xdata.eq(1).count)
tier2 = Numo::DFloat.zeros(xdata.eq(2).count)
count1 = 0
count2 = 0
(xdata.size).times do |i|
  if xdata[i] == 1
      tier1[count1]=ydata[i]
      count1 +=1
  else
      tier2[count2]=ydata[i]
      count2 +=1
  end
end
label1="Tier 1"
label2="Tier 2"
makebiboxplot(fname,xlab,ylab,label1,label2,tier1,tier2)

fname='HighSchoolGraduationYear_Salary'
xlab='High School Graduation Year'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,3]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)
makemultiboxplot(fname,xlab,ylab,xdata,ydata)

fname='Gender_Salary'
xlab='Gender'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,2]
ydata=train_data_n[0..-1,0]
square=0
makeplot(fname,square,xlab,ylab,xdata,ydata)
female = Numo::DFloat.zeros(xdata.eq(0).count)
male   = Numo::DFloat.zeros(xdata.eq(1).count)
fcount = 0
mcount = 0
(xdata.size).times do |i|
  if xdata[i] == 0
      female[fcount]=ydata[i]
      fcount += 1
  else
      male[mcount]=ydata[i]
      mcount +=1
  end
end
labelm="female"
labelf="male"
makebiboxplot(fname,xlab,ylab,labelm,labelf,female,male)

fname='AverageStateGDPperCapita_Salary'
xlab='Average State GDP per Capita (₹)'
ylab='Annual Salary (₹)'
xdata=train_data_n[0..-1,1]
ydata=train_data_n[0..-1,0]
square=1
makeplot(fname,square,xlab,ylab,xdata,ydata)
makemultiboxplot(fname,xlab,ylab,xdata,ydata)

# Fit and test Random Forest model
values  = train_data_n[0..-1,0]
samples = train_data_n[0..-1,1..-1]
training_values  = train_data_n[0..2800,0] 
training_samples = train_data_n[0..2800,1..-1]
test_values  = train_data_n[2801..-1,0]
test_samples = train_data_n[2801..-1,1..-1]
# Use grid search to find best model
rfc = Rumale::Ensemble::RandomForestRegressor.new(
  random_seed:1)
pg = { n_estimators: [5,200], max_depth: [4,9], 
       max_leaf_nodes: [20,150] }
kf = Rumale::ModelSelection::KFold.new(n_splits: 10, 
                                       shuffle: true, random_seed: 1)
gs = Rumale::ModelSelection::GridSearchCV.new(estimator: rfc, 
                                              param_grid: pg, splitter: kf)
gs.fit(training_samples,training_values)

p "Cross validation results"
p gs.cv_results
p "Best parameters"
p gs.best_params
p "Score on Training Set"
p gs.score(training_samples,training_values)
p "Score on Test Set"
p gs.score(test_samples,test_values)
p "Mean square error on Training set"
predicted_salary = gs.predict(training_samples)
evaluator = Rumale::EvaluationMeasure::MeanSquaredError.new
rmse = (evaluator.score(training_values, predicted_salary))**0.5
p rmse
p "Mean square error on Test set"
predicted_salary = gs.predict(test_samples)
evaluator = Rumale::EvaluationMeasure::MeanSquaredError.new
rmse = (evaluator.score(test_values, predicted_salary))**0.5
p rmse

# Fit model again with best parameters to get 
# feature importance
estimator =
  Rumale::Ensemble::RandomForestRegressor.new(
    n_estimators: gs.best_params[:n_estimators], 
    criterion: 'mse', 
    max_depth: gs.best_params[:max_depth], 
    max_leaf_nodes: gs.best_params[:max_leaf_nodes], 
    min_samples_leaf: 5, 
    random_seed: 1)

estimator.fit(samples, values)
p "Feature importances"
importances = estimator.feature_importances
(importances.size).times do |i|
  p headers[i]
  p importances[i]
end

# Evaluate fitting of full dataset
predicted_salary = estimator.predict(samples)
evaluator = Rumale::EvaluationMeasure::MeanSquaredError.new
rmse = (evaluator.score(values, predicted_salary))**0.5
p "Root Mean Squared Error"
p rmse

fname='PredictiedSalary_vs_ActualSalary'
xlab='Actual Salary (₹)'
ylab='Predicted Salary (₹)'
square=1
makeplot(fname,square,xlab,ylab,values,predicted_salary)

error = (100*(predicted_salary - values)/(values+0.1)) 
fname='ErrorInPredictiedSalary_vs_ActualSalary'
xlab='Actual Salary (₹)'
ylab='Percentage Error in Prediction'
square=0
makeplot(fname,square,xlab,ylab,values,error)
